# Application de Facturation Java Backend

Cette application a pour objectif de calculer le montant à facturer à un client pour un mois calendaire. Elle prend en charge deux types de clients :#

#Application de Facturation Java Backend

Cette application a pour objectif de calculer le montant à facturer à un client pour un mois calendaire. Elle prend en charge deux types de clients :

### Clients Professionnels (Pro)
- **Reference Client**: EKL + 9 caractères numériques
- **N° SIRET**
- **Raison Sociale**
- **CA**

### Particuliers
- **Reference Client**: EKL + 9 caractères numériques
- **Civilité**
- **Nom**
- **Prénom**

Un client peut consommer deux types d'énergies :
- Electricité
- Gaz

Les tarifs de facturation au kWh varient selon le type de client et le type d'énergie consommée :
- Pour les particuliers : 0,133 €/kWh pour l'électricité et 0,108 €/kWh pour le gaz.
- Pour les professionnels avec un CA supérieur à 1 000 000 € : 0,110 €/kWh pour l'électricité et 0,123 €/kWh pour le gaz.
- Pour les professionnels avec un CA inférieur à 1 000 000 € : 0,112 €/kWh pour l'électricité et 0,117 €/kWh pour le gaz.


## Fonctionnalités de l'Application
L'application permet de :
- Enregistrer des clients (Pro ou Particuliers) avec leurs informations spécifiques.
- Gérer la consommation en électricité et en gaz pour chaque client.
- Calculer le montant à facturer pour un client donné en fonction de sa consommation et des tarifs définis.
